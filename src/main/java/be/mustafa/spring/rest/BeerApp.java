package be.mustafa.spring.rest;

import be.mustafa.spring.rest.beers.BeerRepository;
import be.mustafa.spring.rest.beers.BeerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true)
public class BeerApp  extends SpringBootServletInitializer { //om dit een web servlet te maken op de server
    public final static String USR = "homer";
    public final static String PSW = "password";
    public final static String QUERY_USERS = "select name, passwordbc, enabled from Users where name = ?";
    public final static String QUERY_AUTHORITY = "select name, role from Users where name = ?";

    public static void main(String[] args) {
        SpringApplication.run(BeerApp.class, args);

        /*BeerRepository repo = ctx.getBean(BeerRepository.class);
        BeerService service = ctx.getBean(BeerService.class);
        System.out.println(repo.getBeerById(4));

        service.orderBeers("an Order2", new int [][] {{4,-10}, {4,-5}, {4,-5}});

        System.out.println(repo.getBeerById(4));*/

    }

    @Bean
    public WebSecurityConfigurer<WebSecurity> securityConfigurer(){
        return new WebSecurityConfigurerAdapter() {
            @Override
            protected void configure(AuthenticationManagerBuilder auth) throws Exception {
                auth.inMemoryAuthentication()
                        .passwordEncoder(new BCryptPasswordEncoder())
                        .withUser("admin")
                        .password("$2a$10$21dWSFmOAdNcTCWx1HvvyugZlytK87E2Xie2aWlXZ8tRv.VReinPW")
                        .roles("ADULT")
                        .and()
                        .withUser("mufasa")
                        .password("$2y$12$g9cHksDaZHo7hvHwhLKPG.imFyPFnnjOqKI.3eO1RtM0a5Pvhq5xy")
                        .roles("ANIMAL");

            }
            @Override
            protected void configure(HttpSecurity http) throws Exception{
                http.csrf().disable();
                http.httpBasic();
                http.authorizeRequests()
                        .antMatchers("/orders/**")
                        .hasRole("ADULT");
            }

        };
    }

    //NODIG OM DIT TE PACKAGEN NAAR SERVER ALS SERVLET
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BeerApp.class);
    }

    @Bean
    public String myString() {
        return "hello World";
    }


}
