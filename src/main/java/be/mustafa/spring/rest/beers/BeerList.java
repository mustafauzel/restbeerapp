package be.mustafa.spring.rest.beers;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
public class BeerList implements Serializable {
    private List<Beer> beerlist;

    public BeerList() {
    }

    public BeerList(List<Beer> beerlist) {
        setBeerlist(beerlist);
    }

    @JsonProperty("Beers")
    @XmlElementWrapper(name = "Beers")
    @XmlElement(name = "Beers")
    public List<Beer> getBeerlist() {
        return beerlist;
    }

    public void setBeerlist(List<Beer> beerlist) {
        this.beerlist = beerlist;
    }
}
