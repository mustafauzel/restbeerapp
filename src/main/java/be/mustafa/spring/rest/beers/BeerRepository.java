package be.mustafa.spring.rest.beers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Transactional
public interface BeerRepository extends JpaRepository<Beer, Integer> {

    default Beer getBeerById(int id) {
        return findById(id).orElse(null);
    }

    @Query(name = "findByAlcohol")
    List<Beer> getBeerByAlcohol(double alcohol);

    @Transactional
    default void updateBeer(Beer b) {
        save(b);
    }

    public List<Beer> findByAlcoholOrderByNameAsc(double alcohol);
    public List<Beer> findBeerByAlcoholBetween(double min, double max);
    public List<Beer> findBeerByAlcoholGreaterThan(double min);
    public List<Beer> findBeerByAlcoholLessThan(double max);
    public List<Beer> findByAlcoholOrName(double alcohol, String name);
    public List<Beer> findByNameLikeOrderByName(String name);
    public List<Beer> findFirst10ByAlcohol(double alcohol);
    public Stream<Beer> findByStockLessThan(int stock);

    List<Beer> getBeersByName(String name);

    @Transactional
    @Modifying
    @Query("update Beer b set b.price=b.price*?1")
    public int updatePrice(double perc);
}
