package be.mustafa.spring.rest.controllers;

import be.mustafa.spring.rest.beers.Beer;
import be.mustafa.spring.rest.beers.BeerList;
import be.mustafa.spring.rest.beers.BeerRepository;
import be.mustafa.spring.rest.beers.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/**/beers")
public class BeerRestController {
    private BeerService service;
    private BeerRepository repo;

    @Autowired
    public void setService(BeerService service) {
        this.service = service;
    }

    @Autowired
    public void setRepo(BeerRepository repo) {
        this.repo = repo;
    }

    @GetMapping(value = "{id:^\\d+$}",
    produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Beer> getBeerById(@PathVariable("id") int id){
        Beer beer = repo.getBeerById(id);
        System.out.println(beer.getName());
        if(beer != null){
            return new ResponseEntity<>(beer, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(params = {"name"},
    produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<BeerList> getBeerByName(@RequestParam(value = "name") String name) {
        List<Beer> beerList = repo.getBeersByName(name);

        if(beerList.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        else{
            return new ResponseEntity<>(new BeerList(beerList), HttpStatus.OK);
        }
    }

    @GetMapping(params = {"alcohol"},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<BeerList> getBeerByAlcohol(@RequestParam(value = "alcohol") double alcohol) {
        List<Beer> beerList = repo.getBeerByAlcohol(alcohol);

        if(beerList.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        else{
            return new ResponseEntity<>(new BeerList(beerList), HttpStatus.OK);
        }
    }
}
