package be.mustafa.spring.rest.controllers;

import be.mustafa.spring.rest.beers.BeerRepository;
import be.mustafa.spring.rest.beers.BeerService;
import be.mustafa.spring.rest.orders.BeerOrder;
import be.mustafa.spring.rest.orders.BeerOrderItem;
import be.mustafa.spring.rest.orders.BeerOrderRepository;
import be.mustafa.spring.rest.orders.OrderInfoSimple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@RestController
@RequestMapping("/orders")
public class OrderRestController {
    private BeerService service;
    private BeerRepository repo;
    private BeerOrderRepository orderRepo;

    @Autowired
    public void setService(BeerService service) {
        this.service = service;
    }

    @Autowired
    public void setRepo(BeerRepository repo) {
        this.repo = repo;
    }

    @Autowired
    public void setOrderRepo(BeerOrderRepository orderRepo) {
        this.orderRepo = orderRepo;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    //@Secured("ADULT")
    public ResponseEntity orderBeers(@RequestBody OrderInfoSimple orderInfo, HttpServletRequest request){
        int orderid = this.service.orderBeer(orderInfo.getName(), orderInfo.getBeerId(),orderInfo.getBeerAmount());
        if(orderRepo.getBeerOrderById(orderid) == null){
            return ResponseEntity.notFound().build();
        }

        URI uri = URI.create(request.getRequestURL() + "/" + orderid);

        //return ResponseEntity.created(uri).build();
        return ResponseEntity.ok(null);
    }

    @GetMapping(
            value = "{id}",
            produces = {
                    MediaType.APPLICATION_JSON_UTF8_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    //@Secured("ADULT")
    public ResponseEntity<BeerOrder> getOrderById(@PathVariable("id") int id) {
        BeerOrder order = orderRepo.findById(id).orElse(null);
        if (order == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return new ResponseEntity<>(order, HttpStatus.OK);
        }
    }
}
